# google-search-results

Created with HTML and CSS as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/web-development-101/). 

# Final Thoughts 

Completing this project allowed me to review HTML and CSS fundamentals and practice with elements such as Flexbox and shapes that can be created with CSS. This project also started to let me see some of the limitations of only using CSS and HTML to create webpages.
